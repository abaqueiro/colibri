package colibri;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.net.URL;

/*
CROSS PLATFORM NOTES:

In windows
	FileUtils.class.getProtectionDomain().getCodeSource().getLocation();
	returns
	a normalized path ej. /C:/A/B/...
	so is not neet to use File.separator but /
*/

public class FileUtils {
	public static boolean DEBUG = false;

	public static String getAppDir(){
		int pos;
		if (DEBUG) System.out.println("getAppDir() called");
		URL url = FileUtils.class.getProtectionDomain().getCodeSource().getLocation();
		String path = url.getPath();
		if (DEBUG) System.out.println("path=[" + path + "]");
		if ( url.getPath().endsWith(".jar") ){
			pos = path.lastIndexOf("/");
			if ( pos>=0 ) path = path.substring(0,pos);
		}
		pos = path.lastIndexOf(File.separator+"WEB-INF"+File.separator);
		if ( pos >= 0 ) path = path.substring(0,pos);
		if (DEBUG) System.out.println("returning path=[" + path + "]");
		return path;
	}

	public static String getAppName(){
		int pos;
		URL url = FileUtils.class.getProtectionDomain().getCodeSource().getLocation();
		String path = url.getPath();
		// quitamos todo el path si hay
		pos = path.lastIndexOf("/");
		if ( pos>=0 ) path = path.substring(pos+1);
		// quitar extension jar si existe
		if ( path.endsWith(".jar") ){
			path = path.substring(0, path.length()-4 );
		}
		return path;
		/*
		String path = getAppDir();
		int pos = path.lastIndexOf(File.separator);
		if ( pos >=0 && pos+1<path.length() ){
			return path.substring(pos+1);
		} else {
			return path;
		}
		*/
	}

	public static String readFile( String fileName ) throws FileNotFoundException, IOException {
		BufferedReader in = new BufferedReader( new FileReader( fileName ) );
		StringBuilder sb = new StringBuilder();
		String l;
		while( ( l=in.readLine() ) != null ){
			sb.append(l);
			sb.append("\n");
		}
		return sb.toString();
	}

}
