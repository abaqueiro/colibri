package colibri;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class GuiUtils {

	public static void centerWindow( JFrame frame ){
		Dimension ws = frame.getSize();
		Dimension ss = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation( (ss.width-ws.width)/2, (ss.height-ws.height)/2 );
	}

	public static GridBagConstraints buildGBC( int row, int col, int rowspan, int colspan, int componentFillDirection, int rowFillFactor, int colFillFactor, int componentOrientation ){
		GridBagConstraints cc = new GridBagConstraints();
		cc.gridy = row;
		cc.gridx = col;
		cc.gridwidth = colspan;
		cc.gridheight = rowspan;
		cc.fill = componentFillDirection;
		cc.weighty = rowFillFactor;
		cc.weightx = colFillFactor;
		cc.anchor = componentOrientation;
		return cc;
	}

}
