package colibri;

import java.util.ArrayList;

public class StringUtils {

	public static String[] explode( String delimiter, String toExplode ){
		String s = toExplode;
		int posi, posf, posx;
		posi = 0;
		posx = s.indexOf( delimiter, posi );
		ArrayList pars = new ArrayList();
		while ( posx!=-1 ){
			pars.add( s.substring(posi,posx) );
			posi = posx + delimiter.length();
			if ( posi > s.length() ) break;
			posx = s.indexOf( delimiter, posi );
		}
		pars.add( s.substring( posi, s.length()) );
		int size = pars.size();
		String[] res = new String[size];
		for(int i=0; i<size; i++){
			res[i] = (String) pars.get(i);
		}
		return res;
	}

}
