/**

La idea es una clase que permita configurar las apps de manera sencilla y automágica

las aplicaciones usuarios deberan hacer el import a esta clase

y hacer el llamado para obtener los valores de la configuración:

si la app esta en un jar de nombre app.jar
el config a buscar es app.cfg en el mismo directorio del app.jar

ConfigLoader.get("key")
	devuelve String, ya sea "" o con un valor, no se devuelve nulo
	porque esto genera más problemas de null pointer exception

*/

package colibri;

import java.io.File;
import java.io.FileInputStream;

import java.util.Properties;

public class ConfigLoader {

	public static final boolean DEBUG = false;

	private static Properties _prop;

	static {
		String configFile;
		System.out.println("ConfigLoader BEGIN ====");
		configFile = FileUtils.getAppDir() + "/" + FileUtils.getAppName() + ".cfg";
		System.out.println("configFile=[" + configFile + "]");
		_prop = new Properties();
		try {
			FileInputStream ins = new FileInputStream( configFile );
			_prop.load( ins );
		} catch ( Exception ex ){
			System.out.println( "ERROR reading config file: " + ex );
		}
		System.out.println("ConfigLoader END ====");
	}

	public static String get( String key ){
		String res;
		if ( DEBUG ) System.out.println( "ConfigLoader.get(\"" + key + "\");" );
		res = _prop.getProperty( key );
		if ( res==null ) res = ""; // reduce complexity or entropy or error oportunity
		if ( DEBUG ) System.out.println( "ConfigLoader.get returns \"" + res  + "\"" );
		return res;
	}

}
